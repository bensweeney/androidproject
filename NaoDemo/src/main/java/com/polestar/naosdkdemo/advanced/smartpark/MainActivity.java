package com.polestar.naosdkdemo.advanced.smartpark;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOLocationHandle;
import com.polestar.naosdk.api.external.NAOSyncListener;
import com.polestar.naosdkdemo.MyNaoService;
import com.polestar.naosdkdemo.ServicesActivity;

/**
 *  Activity synchronizing data (using key set in MyNaoService) and call MapActivity
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // init view: add button to go to Map activity,
        // the app will start location service when the user opens the map
        initViewAndAddButtonToGoToMap();

        // pre-download Positioning Database, so when we go to the map, we can use the local PDB
        downloadNAOResources();

    }


    // init view: add button to go to Map activity,
    // the app will start location service when the user opens the map
    private void initViewAndAddButtonToGoToMap() {
        FrameLayout layout = new FrameLayout(this);
        Button goToMapActivityBtn = new Button(this);
        goToMapActivityBtn.setText("Display Map");
        goToMapActivityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), com.polestar.naosdkdemo.advanced.smartpark.MapActivity.class);
                startActivity(intent);
            }
        });
        layout.addView(goToMapActivityBtn);
        setContentView(layout);
    }

    // pre-download Positioning Database, so when we go to the map, we can use the local PDB
    private void downloadNAOResources() {
        // synchronize PDB
        NAOLocationHandle locationHandle = new NAOLocationHandle(this, MyNaoService.class, ServicesActivity.getKeyFromPrefs(getApplicationContext()), null, null);
        Toast.makeText(getApplicationContext(), "Synchronization starts...", Toast.LENGTH_LONG).show();
        locationHandle.synchronizeData(new NAOSyncListener() {
            @Override
            public void onSynchronizationSuccess() {
                Toast.makeText(getApplicationContext(), "Synchronization Success", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSynchronizationFailure(NAOERRORCODE errorCode, String message) {
                Toast.makeText(getApplicationContext(), "Synchronization Failure: " + message, Toast.LENGTH_LONG).show();
            }
        });
    }

}
