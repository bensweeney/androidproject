package com.polestar.naosdkdemo.advanced.smartpark;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.polestar.helpers.Log;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOGeofencingHandle;
import com.polestar.naosdk.api.external.NAOGeofencingListener;
import com.polestar.naosdk.api.external.NAOLocationHandle;
import com.polestar.naosdk.api.external.NAOLocationListener;
import com.polestar.naosdk.api.external.NAOWakeUpNotifier;
import com.polestar.naosdk.api.external.NaoAlert;
import com.polestar.naosdk.api.external.TNAOFIXSTATUS;
import com.polestar.naosdkdemo.MyNaoService;
import com.polestar.naosdkdemo.ServicesActivity;

/**
 * Class called when user enter/leave the GPS geofence area defined in NAOCloud
 */
public class SmartParkWakeUpNotifier extends NAOWakeUpNotifier implements NAOGeofencingListener, NAOLocationListener {
    // in this example we will try to mock the "walking mode detection"
    // let's say that the user exists the car 10s after he enters the parking
    // time before the user exists the car:
    private static final int TIME_BEFORE_EXIT_CAR = 60;
    private static final String PARKING_SLOT = "J3-E4"; // just a fake parking slot

    private NAOLocationHandle locationHandle; // this is the entry point to pilot location service
    private NAOGeofencingHandle geofencingHandle; // this is the entry point to pilot geofencing service
    private Location carLocation;
    private int nbLocations = 0;


    @Override
    public void onEnterGPSArea(){
        displayNotification("Enter GPS Area");
    }

    @Override
    public void onExitCoverage() {
        stopGeofencing(); // in case we never found an "Enter Park"
        stopLocation(); // in case the user never exits the car
        displayNotification("Exit Coverage Area");
    }

    @Override
    public void onExitGPSArea(){
        displayNotification("Exit GPS Area");
    }

    // called when enter site (not when enter GPS radius), we should start geo-fencing service
    // to detect when the user enters a parking
    // app is still killed (none of the app activities is working)
    @Override
    public void onEnterBeaconArea(){
        displayNotification("Enter Beacon Area");
        displayWelcomeToParkingNotification("Beacon");
        startGeofencing();
    }

    @Override
    // when we detect enter park, we display a welcome popup and start location to track the car
    public void onFireNaoAlert(NaoAlert alert) {

        Log.writeToLog(this.getClass().getName(), "Smartpark new alert " + alert.getName());
        if (alert.getName().contains("Enter_Park")){ //enter park detected
            String parkingName = alert.getName().split("Enter_")[1];
            Log.restricted(getClass().getName(), "Enter park " + parkingName);
            displayWelcomeToParkingNotification(parkingName);
            stopGeofencing(); // no need to detect another enter park
            startLocation(); // to track the car

        }else{
            Log.restricted(getClass().getName(), "Alert fired : " + alert.getName());
        }
    }

    @Override
    public void onLocationChanged(Location loc) {
        Log.alwaysWarn(getClass().getName(), "Smart Park Loc " + loc.getLongitude() + ", " + loc.getLatitude());
        carLocation = loc;
        nbLocations++;
        if (nbLocations >= TIME_BEFORE_EXIT_CAR){
            whenUserExitsCar();
        }
    }

    protected void whenUserExitsCar(){
        if (carLocation != null){
            stopLocation(); // no need to track car location anymore !!! warning: memory leak
            displayParkingSlotNotification(PARKING_SLOT);
        }
    }


    /*********************************************************************
     * Notifications
     *********************************************************************/

    private void displayWelcomeToParkingNotification(String parkingName) {
        String notificationName = "Welcome to " + parkingName;
        String notificationMessage = "This is a smart parking, it can remember your parking slot for you!";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext())
                .setSmallIcon(android.R.drawable.ic_dialog_info) // notification icon
                .setContentTitle(notificationName) // title for notification
                .setContentText(notificationMessage) // message for notification
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true); // clear notification after click

        // set the intent to start the map when click on the notification
        Intent startMapActivityIntent = new Intent(getContext(), MapActivity.class);
        PendingIntent pi = PendingIntent.getActivity(getContext(), 0, startMapActivityIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =(NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // display the notificaiton
        mNotificationManager.notify(0, mBuilder.build());
    }

    private void displayParkingSlotNotification(String parkingSlot){
        String notificationName = "Your parking slot is " + parkingSlot;
        String notificationMessage = "Please click here to confirm.";

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext())
                .setSmallIcon(android.R.drawable.ic_dialog_info) // notification icon
                .setContentTitle(notificationName) // title for notification
                .setContentText(notificationMessage) // message for notification
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true); // clear notification after click

        // set the intent to start the map when click on the notification
        Intent startMapActivityIntent = new Intent(getContext(), MapActivity.class);
        startMapActivityIntent.putExtra(MapActivity.KEY_PARKING_SLOT, parkingSlot);
        PendingIntent pi = PendingIntent.getActivity(getContext(), 0, startMapActivityIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =(NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // display the notificaiton
        mNotificationManager.notify(0, mBuilder.build());
    }

    private void displayNotification(String message) {
        String notificationName = message;
        String notificationMessage = message;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext())
                .setSmallIcon(android.R.drawable.ic_dialog_info) // notification icon
                .setContentTitle(notificationName) // title for notification
                .setContentText(notificationMessage) // message for notification
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true); // clear notification after click

        NotificationManager mNotificationManager =(NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        // display the notificaiton
        mNotificationManager.notify(0, mBuilder.build());
    }

    /*********************************************************************
     * Start stop NAO services
     *********************************************************************/

    private void startGeofencing(){
        if (geofencingHandle == null){
            geofencingHandle = new NAOGeofencingHandle(this.getContext(), MyNaoService.class, ServicesActivity.getKeyFromPrefs(getContext()), this, null);
        }

        Log.writeToLog(this.getClass().getName(), "Smartpark start geofencing");
        geofencingHandle.start();
    }

    private void stopGeofencing(){
        if (geofencingHandle != null){
            Log.writeToLog(this.getClass().getName(), "Smartpark stop geofencing");
            geofencingHandle.stop();
            geofencingHandle = null;
        }
    }

    private void startLocation() {
        if (locationHandle == null){
            Log.writeToLog(this.getClass().getName(), "Smartpark start location");
            locationHandle = new NAOLocationHandle(this.getContext(), MyNaoService.class, ServicesActivity.getKeyFromPrefs(getContext()), this, null);
        }
        locationHandle.start();
    }

    private void stopLocation(){
        carLocation = null;
        nbLocations = 0;
        if (locationHandle != null){
            Log.writeToLog(this.getClass().getName(), "Smartpark stop location");
            locationHandle.stop();
            locationHandle = null;
        }
    }


    /*********************************************************************
     * Ignored callbacks because the app is killed and we'll do nothing
     *********************************************************************/

    @Override
    public void onError(NAOERRORCODE errCode, String message) {
        Log.alwaysError(getClass().getName(), "onError : " + message);
    }

    @Override
    public void onLocationStatusChanged(TNAOFIXSTATUS status) {
        Log.alwaysWarn(getClass().getName(), "onLocationStatusChanged : " + status.toString());
    }

    @Override
    public void onEnterSite(String name) {
        Log.alwaysWarn(getClass().getName(), "onEnterSite : " + name );
    }

    @Override
    public void onExitSite(String name) {
        Log.alwaysWarn(getClass().getName(), "onExitSite : " + name );
    }


}
