package com.polestar.naosdkdemo.advanced.smartpark;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOLocationHandle;
import com.polestar.naosdk.api.external.NAOLocationListener;
import com.polestar.naosdk.api.external.NAOSensorsListener;
import com.polestar.naosdk.api.external.NAOServicesConfig;
import com.polestar.naosdk.api.external.TNAOFIXSTATUS;
import com.polestar.naosdkdemo.MyNaoService;
import com.polestar.naosdkdemo.R;
import com.polestar.naosdkdemo.ServicesActivity;
import com.polestar.naosdkdemo.utils.PermissionsUtils;

/**
 * Activity representing a MapActivity showing current location.
 */
public class MapActivity extends AppCompatActivity implements NAOLocationListener, NAOSensorsListener {
    public static final String KEY_PARKING_SLOT = "smartpark_parking_slot";

    private NAOLocationHandle locationHandle; // this is the entry point to pilot location service
    private TextView map; // let assume this is the map
    private String parkingSlot;

    /************************************************
     * Map activity life cycle
     ***********************************************/

    // init location handle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationHandle = new NAOLocationHandle(this, MyNaoService.class, ServicesActivity.getKeyFromPrefs(getApplicationContext()), this, this);

        // init map, here it's just a text view to display location
        FrameLayout layout = new FrameLayout(this);
        map = new TextView(this);
        layout.addView(map);
        setContentView(layout);


        // verify if we are here when the app has detected the user's car parking slot
        Intent intent = getIntent();
        parkingSlot = intent.getStringExtra(KEY_PARKING_SLOT);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        // this means that the OnEnterSiteListner has detected the user's parking slot
        // and then the user clicks on the "parking slot" popup to confirm
        if (parkingSlot != null){
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(KEY_PARKING_SLOT, parkingSlot);
            editor.commit();
        }else{
            parkingSlot = prefs.getString(KEY_PARKING_SLOT, null);
        }
        if (parkingSlot != null){
            displayParkingSlotDialog();
        }
    }

    // this means that the OnEnterSiteListner has detected the user's parking slot
    // and then the user clicks on the "parking slot" popup to confirm
    private void displayParkingSlotDialog() {
        String message = "Your parking slot is " + parkingSlot + ".";
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton(this.getResources().getString(android.R.string.ok), null)
                .show();
    }

    // configure NAO to detect when the user enters the parking and the app is killed
    private void activateOnSiteWakeUp() {
        NAOServicesConfig.enableOnSiteWakeUp(this, ServicesActivity.getKeyFromPrefs(getApplicationContext()), SmartParkWakeUpNotifier.class, null); // throws an exception when AndroidGeofencingService is not declared in the application Manifest.xml

    }

    // start location when the map is visible
    @Override
    public void onResume(){
        if(null != locationHandle){
            Log.d(getClass().getName(), "Map visible : Start location");

            // Location service needs some permissions to work, in Android 6 we need to ask the user for them, if already granted, we can start location
            // if not granted, we ask for permissions and start location only if permissions granted
            checkPermissionsAndStartLocation();
        }
        super.onResume();
    }

    // stop location when the map is not visible
    @Override
    public void onPause() {
        if(null !=  locationHandle){
            Log.d(getClass().getName(), "Map not visible : Stop location");
            Toast.makeText(getApplicationContext(), "Location stops...", Toast.LENGTH_SHORT).show();
            locationHandle.stop();
        }
        super.onPause();
    }


    /************************************************
     * Permissions related methods for Android 6
     ***********************************************/

    private void onPermissionsGranted(){
        Toast.makeText(getApplicationContext(), "Location starts...", Toast.LENGTH_SHORT).show();
        //we need ACCESS_FINE_LOCATION permission to use on-site wake-up
        activateOnSiteWakeUp();
        locationHandle.start();
    }

    private void onPermissionsRefused(){
        Toast.makeText(getApplicationContext(), "Cannot run the service because permissions have been denied", Toast.LENGTH_LONG).show();
    }

    // call this when you want to start location service
    private void checkPermissionsAndStartLocation(){
        DialogInterface.OnClickListener permissionDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onPermissionsRefused();
            }
        };
        if (PermissionsUtils.checkAndRequest(this, NAOServicesConfig.BASIC_PERMISSIONS, getResources().getString(R.string.permission_msg), 0, permissionDialog)){
            onPermissionsGranted();
        }
    }

    @Override
    // this is called when the user replies to permission requests
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 0){
            boolean allRequestsAccepted = false;
            if (grantResults.length == permissions.length){
                for (int i = 0; i < grantResults.length;  i++){
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED ){
                        allRequestsAccepted = false;
                        break;
                    }else{
                        allRequestsAccepted = true;
                    }
                }
                if (allRequestsAccepted){
                    onPermissionsGranted();
                }else{
                    onPermissionsRefused();
                }
            }
        }
    }


    /*************************************
     * Location related callbacks
     *************************************/

    @Override
    public void onError(NAOERRORCODE errCode, String message) {
        Toast.makeText(getApplicationContext(), "Location : Error : " + message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLocationChanged(Location loc) {
        map.setText("\n\tLocation :\n\t" + loc.getLongitude() + "\n\t" + loc.getLatitude() + "\n\t" + loc.getAltitude() + "\n\t" + loc.getBearing());
    }

    @Override
    public void onLocationStatusChanged(TNAOFIXSTATUS status) {
        Toast.makeText(getApplicationContext(), "Location : Status " + status.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEnterSite(String name) {
        Toast.makeText(getApplicationContext(), "Enter site", Toast.LENGTH_SHORT).show();
        if (parkingSlot == null){
            Toast.makeText(getApplicationContext(), "Start Smart Park", Toast.LENGTH_SHORT).show();
            SmartParkWakeUpNotifier smartPark = new SmartParkWakeUpNotifier();
            smartPark.setContext(this);
            smartPark.onEnterBeaconArea();
        }
    }

    @Override
    public void onExitSite(String name) {
        Toast.makeText(getApplicationContext(), "Exit site", Toast.LENGTH_SHORT).show();
}

    /**************************************
     * Sensor related callbacks
     **************************************/

    @Override
    public void requiresCompassCalibration() {
        Toast.makeText(getApplicationContext(), "Location : please calibrate your phone", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requiresWifiOn() {
        Toast.makeText(getApplicationContext(), "Location : please turn on WIFI", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requiresBLEOn() {
        Toast.makeText(getApplicationContext(), "Location : please turn on Bluetooth", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void requiresLocationOn() {
        Toast.makeText(getApplicationContext(), "Location : please turn on Location", Toast.LENGTH_SHORT).show();
    }
}
