package com.polestar.naosdkdemo.advanced.geofencingonsite;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.polestar.helpers.Log;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOGeofencingHandle;
import com.polestar.naosdk.api.external.NAOGeofencingListener;
import com.polestar.naosdk.api.external.NAOSensorsListener;
import com.polestar.naosdk.api.external.NAOSyncListener;
import com.polestar.naosdk.api.external.NaoAlert;
import com.polestar.naosdk.api.external.TPOWERMODE;
import com.polestar.naosdkdemo.MyNaoService;
import com.polestar.naosdkdemo.ServicesActivity;

public class GeofencingService extends Service  implements NAOGeofencingListener, NAOSensorsListener {

    private String TAG = getClass().getSimpleName();
    private NAOGeofencingHandle naoGeofencingHandle ;

    @Override
    public void onCreate() {
        // create service handle
        naoGeofencingHandle = new NAOGeofencingHandle(this, MyNaoService.class, ServicesActivity.getKeyFromPrefs(getApplicationContext()), this, this);
        // set power mode to low because service is used in background
        naoGeofencingHandle.setPowerMode(TPOWERMODE.LOW);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.alwaysWarn(getClass().getName(), "Service started");

        naoGeofencingHandle.synchronizeData(new NAOSyncListener() {
            @Override
            public void onSynchronizationSuccess() {

            }

            @Override
            public void onSynchronizationFailure(NAOERRORCODE errorCode, String message) {

            }
        });
        //start geofencing service
        if (naoGeofencingHandle.start())
            showNotification(TAG, "Started", true);

        return START_NOT_STICKY;
    }


    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        if(naoGeofencingHandle != null){
            naoGeofencingHandle.stop();
        }
        showNotification(TAG, "Service stopped", true);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onError(NAOERRORCODE errCode, String message) {
        showNotification(TAG, "Error: "+errCode+" "+message, true);
    }

    @Override
    public void onFireNaoAlert(NaoAlert alert) {
        showNotification(TAG, alert.getName(), true);


    }

    public void showNotification(String title, String message, boolean setSound){
        //Build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(message) // message for notification
                .setAutoCancel(true); // clear notification after click
        if(setSound)
            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        // display the notification
        NotificationManager mNotificationManager =(NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());


    }

    /**
     *  Ask user to turn on Bluetooth sensor
     */
    private void showNotificationBluetoothSetting(){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info) // notification icon
                .setContentTitle(TAG) // title for notification
                .setContentText("Please enable Bluetooth") // message for notification
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true); // clear notification after click

        // set the intent to start the map when click on the notification
        Intent enableBtIntent =  new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        PendingIntent pi = PendingIntent.getActivity(this, 0, enableBtIntent, 0);
        mBuilder.setContentIntent(pi);
        NotificationManager mNotificationManager =(NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        // display the notification
        mNotificationManager.notify(1, mBuilder.build());
    }

    @Override
    public void requiresCompassCalibration() {

    }

    @Override
    public void requiresWifiOn() {

    }

    @Override
    public void requiresBLEOn() {
        //ask user to enable BLE
        showNotificationBluetoothSetting();
    }

    @Override
    public void requiresLocationOn() {

    }
}
