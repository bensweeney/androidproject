package com.polestar.naosdkdemo.examples;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOSensorsListener;
import com.polestar.naosdk.api.external.NAOServiceHandle;
import com.polestar.naosdk.api.external.NAOSyncListener;
import com.polestar.naosdkdemo.R;
import com.polestar.naosdkdemo.ServicesActivity;
import com.polestar.naosdkdemo.utils.ColorUtils;
import com.polestar.naosdkdemo.utils.PermissionsUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Generic class holding functionality common to all service clients.
 * In particular, it implements NAOSensorsListener to receive notification about sensor requirements
 * needed by a particular service (for example: Bluetooth activation).
 */
public abstract class AbstractClient<ServiceHandle extends NAOServiceHandle> extends AppCompatActivity implements NAOSensorsListener, View.OnClickListener{

    protected static final String EMULATOR_KEY = "emulator";

    protected ServiceHandle serviceHandle; // generic service handle
    protected String apiKeyUsed ;


    /**
     * ===== SENSORS LISTENER CALLBACKS  =======
     */

    @Override
    public void requiresCompassCalibration() { notifyUser("please calibrate Compass"); }

    @Override
    public void requiresWifiOn() {
        notifyUser("please turn on Wifi");
    }

    @Override
    public void requiresBLEOn() {
        notifyUser("please turn on Bluetooth");
    }

    @Override
    public void requiresLocationOn() {
        notifyUser("please turn on Location");
    }

    /**
     * ===== PERMISSION CHECKING =======
     */

    private static final int PERMISSION_REQUEST_CODE = 0;

    private static final String[] NEEDDED_PERMISSIONS = new String[] {
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    protected boolean checkPermissions(){
        return PermissionsUtils.checkAndRequest(this, NEEDDED_PERMISSIONS, getResources().getString(R.string.permission_msg), PERMISSION_REQUEST_CODE,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onPermissionsRefused();
                    }
                });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST_CODE){
            boolean allRequestsAccepted = false;
            if (grantResults.length == permissions.length){
                for (int i = 0; i < grantResults.length;  i++){
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED ){
                        allRequestsAccepted = false;
                        break;
                    }else{
                        allRequestsAccepted = true;
                    }
                }
                if (allRequestsAccepted){
                    startService();
                }else{
                    onPermissionsRefused();
                }
            }
        }
    }

    private void onPermissionsRefused(){
        notifyUser("Cannot run the service because permissions have been denied");
        setNaoServiceStarted(false);

        startButton.setEnabled(false);
        syncButton.setEnabled(false);
        startButton.setBackgroundColor(ColorUtils.getColor(this, R.color.disabledButton));
        syncButton.setBackgroundColor(ColorUtils.getColor(this, R.color.disabledButton));
        syncStateTextView.setText("No sync in progress");
    }

    /**
     * ===== NAO Data Synchronization =======
     */

    protected NAOSyncListener naoSyncListener;

    protected void synchronizeData() {

        if(null == serviceHandle){
            createHandle();
        }

        syncStateTextView.setText("Sync in progress");
        serviceHandle.synchronizeData(naoSyncListener);
    }


    /**
     * ===== ACTIVITY LIFECYCLE and UI =======
     */

    protected boolean isServiceStarted;

    protected Button startButton;
    protected Button syncButton;
    protected Switch emulatorSwitch;
    protected TextView serviceStateTextView;
    protected TextView syncStateTextView;


    protected abstract void createHandle();

    protected abstract boolean hasEmulator();

    protected void startService() {

        if(checkPermissions()){
            // init service
            if (serviceHandle == null) {
                createHandle();

            }

            if(serviceHandle.start())
                setNaoServiceStarted(true);
        }

    }

    protected void stopService() {
        if(serviceHandle != null)
            serviceHandle.stop();

        setNaoServiceStarted(false);
    }

    /**
    * ===== Activity lifecycle =====
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView title = (TextView)findViewById(R.id.service_title);
        title.setText("NAO"+getClass().getSimpleName());

        naoSyncListener = new NAOSyncListener() {

            @Override
            public void onSynchronizationSuccess() {
                Log.d(this.getClass().getName(), "onSynchronizationSuccess");
                notifyUser("onSynchronizationSuccess");
                syncStateTextView.setText("Sync succeeded");
            }

            @Override
            public void onSynchronizationFailure(NAOERRORCODE errorCode, String message) {
                notifyUser("onSynchronizationFailure: " + message);
                Log.d(this.getClass().getName(), "onSynchronizationFailure: " + message);
                syncStateTextView.setText("Sync failed");
            }
        };

        startButton = (Button) findViewById(R.id.startButton);
        syncButton = (Button) findViewById(R.id.syncButton);

        emulatorSwitch = (Switch) findViewById(R.id.emulator_switch);
        emulatorSwitch.setEnabled(hasEmulator());
        emulatorSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    apiKeyUsed = EMULATOR_KEY;
                } else {
                    apiKeyUsed = ServicesActivity.getKeyFromPrefs(getApplicationContext());
                }
                stopService();
                serviceHandle = null;
            }
        });

        serviceStateTextView = (TextView) findViewById(R.id.serviceState);
        syncStateTextView = (TextView) findViewById(R.id.syncState);

//        Init api key:
        apiKeyUsed = ServicesActivity.getKeyFromPrefs(getApplicationContext());
        if(apiKeyUsed.isEmpty() && hasEmulator()){
//           no api key configured: set emulator if available
            emulatorSwitch.setChecked(hasEmulator());
        }
        else{
            emulatorSwitch.setChecked(false);
        }

        startButton.setOnClickListener(this);
        startButton.setEnabled(true);
        startButton.setBackgroundColor(ColorUtils.getColor(this, R.color.enabledButton));

        syncButton.setOnClickListener(this);
        syncButton.setEnabled(true);
        syncButton.setBackgroundColor(ColorUtils.getColor(this, R.color.enabledButton));

        updateServiceStateTextView(false);// display text saying that the service is not started

        syncStateTextView.setText("No sync in progress");

        setNaoServiceStarted(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startButton:
                if (!isServiceStarted) {
                    startService();
                }else{
                    stopService();
                }
                break;
            case R.id.syncButton:
                synchronizeData();
                break;
        }
    }

    private void updateServiceStateTextView(boolean serviceStarted) {
        if (serviceStarted) {
            serviceStateTextView.setText(R.string.service_started);
        } else {
            serviceStateTextView.setText(R.string.service_stopped);
        }
    }

    /**
     * Display message on screen to user
     * @param msg text to display
     */
    public void notifyUser(String msg){
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void setNaoServiceStarted(boolean naoStarted) {
        startButton.setEnabled(true);
        if (naoStarted) {
            startButton.setText(getResources().getString(R.string.stop_button));
        } else {
            startButton.setText(getResources().getString(R.string.start_button));
        }

        if (naoStarted) {
            // Case where naoStarted==true is properly handled in
            // ServiceStarted, so that the message is displayed when the
            // service is actually started. We use the starting message here
            // instead.
            serviceStateTextView.setText(R.string.service_starting);
        } else {
            updateServiceStateTextView(naoStarted);
        }
        isServiceStarted = naoStarted;
    }


    public static void showNotification(Context ctxt, String title, String message, boolean setSound){

        //Get date
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();

        //Build notification
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctxt)
                .setSmallIcon(android.R.drawable.ic_dialog_info) // notification icon
                .setContentTitle(title) // title for notification
                .setContentText(dateFormat.format(date)+"\n"+message) // message for notification
                .setAutoCancel(true); // clear notification after click
        if(setSound)
            mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);


        // display the notification
        NotificationManager mNotificationManager =(NotificationManager) ctxt.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify((int) System.currentTimeMillis(), mBuilder.build());

    }


}
