package com.polestar.naosdkdemo.examples;

import android.util.Log;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOGeofencingHandle;
import com.polestar.naosdk.api.external.NAOGeofencingListener;
import com.polestar.naosdk.api.external.NaoAlert;
import com.polestar.naosdk.api.external.TPOWERMODE;
import com.polestar.naosdkdemo.MyNaoService;


/**
 * Generic class holding functionality common to all Geofencing service clients.
 * It creates service handle and implements NAOGeofencingListener to receive alert notifications
 */
public class GeofencingClient extends AbstractClient<NAOGeofencingHandle> implements NAOGeofencingListener {


    @Override
    protected void createHandle() {
        serviceHandle = new NAOGeofencingHandle(this, MyNaoService.class, apiKeyUsed, this, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
        if(null != serviceHandle) {
            Log.d(getClass().getName(), "onResume -- POWER MODE HIGH");
            serviceHandle.setPowerMode(TPOWERMODE.HIGH);
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
        if(null != serviceHandle) {
            Log.d(getClass().getName(), "onPause -- POWER MODE LOW");
            serviceHandle.setPowerMode(TPOWERMODE.LOW);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // The activity is about to be destroyed.
        if(null != serviceHandle){
            Log.d(getClass().getName(), "onDestroy -- stop location service");
            serviceHandle.stop();
            serviceHandle = null;
        }
    }

    /**
     * Enable emulator mode for Geofencing
     */
    @Override
    protected boolean hasEmulator() {
        return true;
    }

    @Override
    public void onError(NAOERRORCODE errCode, String message) {
        String errorMsg = "onError " + errCode + " " + message;
        Log.e(this.getClass().getName(), errorMsg);
        notifyUser(errorMsg);
    }

    @Override
    public void onFireNaoAlert(NaoAlert alert) {
        Log.d(this.getClass().getName(), "onFireNaoAlert: alert.name " + alert.getName() + " alert.content: " + alert.getContent());
        showNotification(getApplicationContext(),getClass().getSimpleName(), alert.getName(), true);
        serviceStateTextView.setText("Last alert received: " + alert.getName());
    }
}
