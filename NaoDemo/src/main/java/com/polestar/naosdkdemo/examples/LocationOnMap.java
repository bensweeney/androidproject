package com.polestar.naosdkdemo.examples;

import android.util.Log;


/**
 * Class showing how location service should be used for start location only when the application is visible
 */
public class LocationOnMap extends AbstractLocationClient {


    /**
     * Enable emulator for this client
     */
    @Override
    protected boolean hasEmulator(){
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();

        // The activity has become visible (it is now "resumed").
        if(null != serviceHandle && isServiceStarted){
            Log.d(getClass().getName(), "onResume -- Start location service");
            if(checkPermissions())
                serviceHandle.start();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        // Another activity is taking focus (this activity is about to be "paused").
        if(null !=  serviceHandle){
            Log.d(getClass().getName(), "onPause -- stop location service");
            serviceHandle.stop();
        }
    }


}
