package com.polestar.naosdkdemo.examples;

import android.util.Log;

import com.polestar.naosdk.api.external.NAOBeaconReportingHandle;
import com.polestar.naosdk.api.external.NAOBeaconReportingListener;
import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOServicesConfig;
import com.polestar.naosdkdemo.MyNaoService;

/**
 * Class showing how BeaconReportingClient service should be used for uses cases when running in background is needed
 */
public class BeaconReportingClient extends AbstractClient<NAOBeaconReportingHandle> implements NAOBeaconReportingListener {


    @Override
    protected void createHandle()
    {
        serviceHandle = new NAOBeaconReportingHandle(this, MyNaoService.class, apiKeyUsed, this, this);
    }

    @Override
    protected boolean hasEmulator() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the service when activity is about to be destroyed.
        if(null != serviceHandle){
            serviceHandle.stop();
            serviceHandle = null;
        }
    }

    /**
     * ===== OVERRIDE LISTENER CALLBACKS =======
     */

    @Override
    public void onError(NAOERRORCODE errCode, String msg) {
        Log.e(this.getClass().getName(), "onError " + msg);
        notifyUser("onError " + errCode + " " + msg);
    }

}
