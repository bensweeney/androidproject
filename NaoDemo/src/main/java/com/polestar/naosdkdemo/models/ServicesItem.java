package com.polestar.naosdkdemo.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent service item which contains use case
 * Used in expandable list view
 */
public class ServicesItem {

    private String name ;
    private List<UseCaseItem> childItemList ;

    public ServicesItem(String name){
        this.name = name;
        childItemList = new ArrayList<>();
    }

    public List<UseCaseItem> getChildItemList(){
        return childItemList;
    }

    public String getName() {
        return name;
    }
}
